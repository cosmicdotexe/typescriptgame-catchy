var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Game = (function () {
    function Game(bpm) {
        var _this = this;
        this.debug = true;
        this._container = document.getElementById('container');
        this._timerToken = 0;
        this._requestTicks = -20;
        this.handleEvt = function (e) {
            if (e.keyCode === 39) {
                _this._platform1.moveRelative(130, 0);
                if (_this._platform1.rightbounds + 150 > (window.screen.width)) {
                    _this._platform1.moveRelative(-130, 0);
                }
            }
            else if (e.keyCode === 37) {
                _this._platform1.moveRelative(-130, 0);
                if (_this._platform1.leftbounds < (0.5 * window.screen.width + 100)) {
                    _this._platform1.moveRelative(130, 0);
                }
            }
            if (e.keyCode === 68) {
                _this._platform2.moveRelative(130, 0);
                if (_this._platform2.rightbounds + 150 > (0.5 * window.screen.width)) {
                    _this._platform2.moveRelative(-130, 0);
                }
            }
            else if (e.keyCode === 65) {
                _this._platform2.moveRelative(-130, 0);
                if (_this._platform2.leftbounds < (100)) {
                    _this._platform2.moveRelative(130, 0);
                }
            }
        };
        this._requestTicks = -20;
        this._totalRequestTicks = 0;
        this._noteArrayBlue = [];
        this._noteArrayOrange = [];
        this._bpm = bpm;
        document.addEventListener("keydown", this.handleEvt);
        this._platform1 = new Platform("platform", 50, -10);
        this._platform2 = new Platform("platform", 0, -10);
        this._refreshIntervalId = setInterval(function () { return _this.createNote(); }, 50);
        this._combop1 = 1;
        this._combop2 = 1;
        this._livesp1 = 3;
        this._livesp2 = 3;
        this._scorep1 = 0;
        this._scorep2 = 0;
    }
    Game.prototype.createNote = function () {
        this._requestTicks++;
        this._totalRequestTicks++;
        if (this._requestTicks == 10) {
            this._noteArrayOrange.push(new Note("note", (Math.random() * (95 - 55) + 55) - 2, 0, 230));
            this._noteArrayBlue.push(new Note("note2", (Math.random() * (45 - 5) + 5) - 2, 0, 230));
            this._timerToken++;
            this._requestTicks = 0;
        }
        this.update();
        if (this._totalRequestTicks > 1200) {
            clearInterval(this._refreshIntervalId);
            if (this._scorep1 > this._scorep2) {
            }
            else {
            }
        }
        else {
            console.log(this._totalRequestTicks);
        }
    };
    Game.prototype.update = function () {
        var selectedObjectOrange;
        var selectedObjectBlue;
        for (var index = 0; index < this._noteArrayBlue.length; index++) {
            selectedObjectBlue = this._noteArrayBlue[index];
            var overlap = (this._platform2.getRect.right > selectedObjectBlue.getRect.right &&
                this._platform2.getRect.left < selectedObjectBlue.getRect.left &&
                this._platform2.getRect.top < selectedObjectBlue.getRect.bottom &&
                this._platform2.getRect.bottom > selectedObjectBlue.getRect.top);
            if (overlap) {
                this._noteArrayBlue.splice(index, 1);
                this._container.removeChild(selectedObjectBlue.getElement);
                this._scorep1 = this._scorep1 + (100 * this._combop1);
                this._combop1++;
                this.updateScoreBoard();
            }
        }
        for (var index = 0; index < this._noteArrayOrange.length; index++) {
            selectedObjectOrange = this._noteArrayOrange[index];
            var overlap = (this._platform1.getRect.right > selectedObjectOrange.getRect.right &&
                this._platform1.getRect.left < selectedObjectOrange.getRect.left &&
                this._platform1.getRect.top < selectedObjectOrange.getRect.bottom);
            if (overlap) {
                this._noteArrayOrange.splice(index, 1);
                this._container.removeChild(selectedObjectOrange.getElement);
                this._scorep2 = this._scorep2 + (100 * this._combop2);
                this._combop2++;
                this.updateScoreBoard();
            }
        }
        for (var index = 0; index < this._noteArrayBlue.length; index++) {
            if (this._noteArrayBlue[index].getRect.top > window.screen.height) {
                selectedObjectBlue = this._noteArrayBlue[index];
                this._noteArrayBlue.splice(index, 1);
                this._container.removeChild(selectedObjectBlue.getElement);
                this._combop1 = 1;
                this._livesp1 = this._livesp1 - 1;
                this.updateScoreBoard();
                if (this._livesp1 <= 0) {
                    this.playerWins(false);
                }
            }
        }
        for (var index = 0; index < this._noteArrayOrange.length; index++) {
            if (this._noteArrayOrange[index].getRect.top > window.screen.height) {
                selectedObjectOrange = this._noteArrayOrange[index];
                this._noteArrayOrange.splice(index, 1);
                this._container.removeChild(selectedObjectOrange.getElement);
                this._combop2 = 1;
                this._livesp2 = this._livesp2 - 1;
                this.updateScoreBoard();
                if (this._livesp2 <= 0) {
                    this.playerWins(true);
                }
            }
        }
    };
    Game.prototype.playerWins = function (isPlayerOne) {
        if (isPlayerOne) {
            var h1 = document.createElement('h1');
            h1.className = "player1win";
            h1.innerHTML = "Player 1 Wins!";
            this._container.appendChild(h1);
            clearInterval(this._refreshIntervalId);
        }
        else {
            var h1 = document.createElement('h1');
            h1.className = "player2win";
            h1.innerHTML = "Player 2 Wins!";
            this._container.appendChild(h1);
            clearInterval(this._refreshIntervalId);
        }
    };
    Game.prototype.updateScoreBoard = function () {
        var comboboard = document.getElementById("player1combo");
        comboboard.innerHTML = this._combop1 + "X";
        var scoreboard = document.getElementById("player1score");
        scoreboard.innerHTML = this._scorep1.toString();
        var comboboard2 = document.getElementById("player2combo");
        comboboard2.innerHTML = this._combop2 + "X";
        var scoreboard2 = document.getElementById("player2score");
        scoreboard2.innerHTML = this._scorep2.toString();
    };
    return Game;
}());
var GameItem = (function () {
    function GameItem(name, xPosition, yPosition) {
        if (xPosition === void 0) { xPosition = 0; }
        if (yPosition === void 0) { yPosition = 0; }
        this._container = document.getElementById('container');
        this._name = name;
        this._xPos = xPosition;
        this._yPos = yPosition;
    }
    GameItem.prototype.draw = function (container, isPercentage) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        if (isPercentage) {
            this._xPos = (this._xPos / 100) * window.screen.width;
            this._yPos = (this._yPos / 100) * window.screen.height;
            this._element.style.transform = "translate(" + this._xPos + "px, " + this._yPos + "px)";
        }
        else {
            this._element.style.transform = "translate(" + this._xPos + "px, " + this._yPos + "px)";
        }
        this.image = document.createElement('img');
        this.image.src = "./assets/images/" + this._name + ".png ";
        this.image.className = "" + this._name;
        this._element.appendChild(this.image);
        container.appendChild(this._element);
    };
    GameItem.prototype.moveRelative = function (_xMove, _yMove) {
        this._xPos = this._xPos + _xMove;
        this._yPos = this._yPos + _yMove;
        this._element.style.transform = "translate(" + this._xPos + "px, " + this._yPos + "px)";
    };
    GameItem.prototype.moveAbsolute = function (_xMove, _yMove) {
        this._xPos = _xMove;
        this._yPos = _yMove;
        this._element.style.transform = "translate(" + this._xPos + "px, " + this._yPos + "px)";
    };
    return GameItem;
}());
var app;
var gameState = false;
function start() {
    if (!gameState) {
        app = new Game(160);
        gameState = true;
    }
    else {
        location.reload();
    }
}
var Note = (function (_super) {
    __extends(Note, _super);
    function Note(name, xPosition, yPosition, bpm) {
        if (xPosition === void 0) { xPosition = 0; }
        if (yPosition === void 0) { yPosition = 0; }
        var _this = _super.call(this, name, xPosition, yPosition) || this;
        _this._bpm = bpm;
        _this.draw(_this._container, true);
        setInterval(function () { return _this.update(); }, 25);
        return _this;
    }
    Note.prototype.update = function () {
        this.moveRelative(0, 20);
    };
    Object.defineProperty(Note.prototype, "getRect", {
        get: function () {
            return this._element.getBoundingClientRect();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Note.prototype, "getElement", {
        get: function () {
            return this._element;
        },
        enumerable: true,
        configurable: true
    });
    return Note;
}(GameItem));
var Platform = (function (_super) {
    __extends(Platform, _super);
    function Platform(name, xPosition, yPosition) {
        var _this = _super.call(this, name, xPosition, yPosition) || this;
        _this.draw(_this._container, true);
        return _this;
    }
    Object.defineProperty(Platform.prototype, "leftbounds", {
        get: function () {
            return this._element.getBoundingClientRect().left;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Platform.prototype, "rightbounds", {
        get: function () {
            return this._element.getBoundingClientRect().right;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Platform.prototype, "getRect", {
        get: function () {
            return this.image.getBoundingClientRect();
        },
        enumerable: true,
        configurable: true
    });
    return Platform;
}(GameItem));
//# sourceMappingURL=main.js.map