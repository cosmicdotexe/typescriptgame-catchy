class GameItem {
    protected _container: HTMLElement = document.getElementById('container');
    protected _element: HTMLElement;
    protected _name: string;
    protected _xPos: number;
    protected _yPos: number;
    protected image: HTMLImageElement;

    constructor(name: string, xPosition: number = 0, yPosition: number = 0) {
        this._name = name
        this._xPos = xPosition;
        this._yPos = yPosition;
    }

    public draw(container: HTMLElement, isPercentage: boolean) { //Used for drawing gameitems
        this._element = document.createElement('div');
        this._element.className = this._name;
        if (isPercentage) { //equation for handling percentages.
            this._xPos = (this._xPos / 100) * window.screen.width;
            this._yPos = (this._yPos / 100) * window.screen.height;
            this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        } else {
            this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        }


        this.image = document.createElement('img');
        this.image.src = `./assets/images/${this._name}.png `;
        this.image.className = `${this._name}`;

        this._element.appendChild(this.image);
        container.appendChild(this._element);
    }

    public moveRelative(_xMove: number, _yMove: number) { //moves gameitem relative from current position
        this._xPos = this._xPos + _xMove;
        this._yPos = this._yPos + _yMove;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }

    public moveAbsolute(_xMove: number, _yMove: number) { //moves gameitem to screen coordinates
        this._xPos = _xMove;
        this._yPos = _yMove;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }


}


