/// <reference path="gameItem.ts" />

class Platform extends GameItem {
    constructor(name: string, xPosition: number, yPosition: number) {
        super(name, xPosition, yPosition);

        this.draw(this._container, true);
    }

    get leftbounds(): number {
        return this._element.getBoundingClientRect().left; //gets leftbounds
    }

    get rightbounds(): number {
        return this._element.getBoundingClientRect().right; //gets rightbounds
    }

    get getRect(): any {
        return this.image.getBoundingClientRect(); //gets all position info
    }

}