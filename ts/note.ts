/// <reference path="gameItem.ts" />
class Note extends GameItem {
    private _bpm: number;
    constructor(name: string, xPosition: number = 0, yPosition: number = 0, bpm: number) {
        super(name, xPosition, yPosition);
        this._bpm = bpm;
        this.draw(this._container, true);
        setInterval(() => this.update(), 25); //Update interval
    }

    public update() {
        this.moveRelative(0, 20) //moves notes
    }

    get getRect(): any { //returns note positions and size
        return this._element.getBoundingClientRect();
    }

    get getElement(): HTMLElement { //returns note element.
        return this._element;
    }

}