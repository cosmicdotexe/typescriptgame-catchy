class Game {
  private debug: boolean = true;
  private _platform1: Platform;
  private _platform2: Platform;
  private _combop1: number;
  private _combop2: number;
  private _scorep1: number;
  private _scorep2: number;
  private _livesp1: number;
  private _livesp2: number;
  private _note: Note;
  private _container: HTMLElement = document.getElementById('container');
  private _timerToken: number = 0;
  private _bpm: number;
  private _noteArrayBlue: Note[];
  private _noteArrayOrange: Note[];
  private _update: boolean;
  private _requestTicks: number = -20;
  private _totalRequestTicks: number;
  private _refreshIntervalId: any;

  constructor(bpm: number) {
    this._requestTicks = -20;
    this._totalRequestTicks = 0;
    this._noteArrayBlue = [];
    this._noteArrayOrange = [];
    this._bpm = bpm;
    document.addEventListener("keydown", this.handleEvt); //Listens for keystrokes
    this._platform1 = new Platform("platform", 50, -10); //creates platforms
    this._platform2 = new Platform("platform", 0, -10);
    this._refreshIntervalId = setInterval(() => this.createNote(), 50); //Creates game tick loop
    this._combop1 = 1;
    this._combop2 = 1;
    this._livesp1 = 3;
    this._livesp2 = 3;
    this._scorep1 = 0;
    this._scorep2 = 0;
  }

  public createNote() { //creates notes and handles time related issues
    this._requestTicks++;
    this._totalRequestTicks++;
    if (this._requestTicks == 10) {
      this._noteArrayOrange.push(new Note("note", (Math.random() * (95 - 55) + 55) - 2, 0, 230));
      this._noteArrayBlue.push(new Note("note2", (Math.random() * (45 - 5) + 5) - 2, 0, 230));
      this._timerToken++;
      this._requestTicks = 0;
    }
    this.update();
    if (this._totalRequestTicks > 1200) {
      clearInterval(this._refreshIntervalId); //Timer to end game
      if (this._scorep1 > this._scorep2) {

      } else {

      }
    } else {
      console.log(this._totalRequestTicks);
    }

  }

  public update() {
    let selectedObjectOrange;
    let selectedObjectBlue;
    for (let index = 0; index < this._noteArrayBlue.length; index++) { //Checks note hits for left side of screen.
      selectedObjectBlue = this._noteArrayBlue[index]; //Checks blue notes
      let overlap = (this._platform2.getRect.right > selectedObjectBlue.getRect.right &&
        this._platform2.getRect.left < selectedObjectBlue.getRect.left &&
        this._platform2.getRect.top < selectedObjectBlue.getRect.bottom &&
        this._platform2.getRect.bottom > selectedObjectBlue.getRect.top)
      if (overlap) { //If hit
        this._noteArrayBlue.splice(index, 1);
        this._container.removeChild(selectedObjectBlue.getElement);
        this._scorep1 = this._scorep1 + (100 * this._combop1);
        this._combop1++;
        this.updateScoreBoard();
      }
    }
    for (let index = 0; index < this._noteArrayOrange.length; index++) { //Checks note hits for right side screen.
      selectedObjectOrange = this._noteArrayOrange[index];//Checks orange notes
      let overlap = (this._platform1.getRect.right > selectedObjectOrange.getRect.right &&
        this._platform1.getRect.left < selectedObjectOrange.getRect.left &&
        this._platform1.getRect.top < selectedObjectOrange.getRect.bottom)
      if (overlap) { //If hit
        this._noteArrayOrange.splice(index, 1);
        this._container.removeChild(selectedObjectOrange.getElement);
        this._scorep2 = this._scorep2 + (100 * this._combop2);
        this._combop2++;
        this.updateScoreBoard();
      }
    }

    for (let index = 0; index < this._noteArrayBlue.length; index++) { //Checks misses for left side screen.
      if (this._noteArrayBlue[index].getRect.top > window.screen.height) {
        selectedObjectBlue = this._noteArrayBlue[index];
        this._noteArrayBlue.splice(index, 1);
        this._container.removeChild(selectedObjectBlue.getElement);
        // Miss combo reset and life management player 1
        this._combop1 = 1;
        this._livesp1 = this._livesp1 - 1;
        this.updateScoreBoard();
        if (this._livesp1 <= 0) { //win message
          this.playerWins(false);
        }
      }
    }
    for (let index = 0; index < this._noteArrayOrange.length; index++) { //Checks misses for right side screen
      if (this._noteArrayOrange[index].getRect.top > window.screen.height) {
        selectedObjectOrange = this._noteArrayOrange[index];
        this._noteArrayOrange.splice(index, 1);
        this._container.removeChild(selectedObjectOrange.getElement);
        // Miss combo reset and life management player 2
        this._combop2 = 1;
        this._livesp2 = this._livesp2 - 1;
        this.updateScoreBoard();
        if (this._livesp2 <= 0) { //win message
          this.playerWins(true);
        }
      }
    }
  }

  public playerWins(isPlayerOne: boolean) {
    if (isPlayerOne) {
      let h1 = document.createElement('h1');
      h1.className = "player1win";
      h1.innerHTML = "Player 1 Wins!";
      this._container.appendChild(h1);
      clearInterval(this._refreshIntervalId);
    } else {
      let h1 = document.createElement('h1');
      h1.className = "player2win";
      h1.innerHTML = "Player 2 Wins!";
      this._container.appendChild(h1);
      clearInterval(this._refreshIntervalId);
    }
  }

  public updateScoreBoard() {
    //player1 combo
    let comboboard = document.getElementById("player1combo");
    comboboard.innerHTML = this._combop1 + "X";
    //player1 score
    let scoreboard = document.getElementById("player1score");
    scoreboard.innerHTML = this._scorep1.toString();
    //player2 combo
    let comboboard2 = document.getElementById("player2combo");
    comboboard2.innerHTML = this._combop2 + "X";
    //player1 score
    let scoreboard2 = document.getElementById("player2score");
    scoreboard2.innerHTML = this._scorep2.toString();
  }

  public handleEvt = (e: KeyboardEvent) => { //player 2
    if (e.keyCode === 39) { //right movement, right side screen
      this._platform1.moveRelative(130, 0);
      if (this._platform1.rightbounds + 150 > (window.screen.width)) {
        this._platform1.moveRelative(-130, 0);
      }
    } else if (e.keyCode === 37) { //left movement, right side screen
      this._platform1.moveRelative(-130, 0);
      if (this._platform1.leftbounds < (0.5 * window.screen.width + 100)) {
        this._platform1.moveRelative(130, 0);
      }
    }//player 1
    if (e.keyCode === 68) { //right movement, left side screen
      this._platform2.moveRelative(130, 0);
      if (this._platform2.rightbounds + 150 > (0.5 * window.screen.width)) {
        this._platform2.moveRelative(-130, 0);
      }
    } else if (e.keyCode === 65) { //left movement, left side screen
      this._platform2.moveRelative(-130, 0);
      if (this._platform2.leftbounds < (100)) {
        this._platform2.moveRelative(130, 0);
      }
    }
  }
}