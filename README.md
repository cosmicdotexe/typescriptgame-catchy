# README #

Catchy    
==========================

General Gameplay
--------------------------
Catchy is een spel waar je objecten moet opvangen met je platform, hiervoor krijg je punten. 
Bij elk object dat je vangt krijg je er 1 combo score bij, de combo score vermenigvuldigd de komende noten die je raakt. 
Zodra je mist wordt je combo terug gezet naar 1. Je mag 3 keer missen,
als jij of je tegenstander de 0 aanraakt wint het tegenovergestelde team.
Je hebt in totaal ongeveer 1 minuut om zo veel mogelijk score te verzamelen, na die 1 minuut zal de score bepalen wie er de overwinner is.
Veel success

Controls
--------------------------
Player 1 gebruikt 'a' en 'd'
Player 2 gebruikt '⬅' en '⮕'

UML Klassediagram
--------------------------
![UML](https://i.imgur.com/kCuGpPv.png)



